![Forte Payment Systems](http://www.forte.net/images/forteLogo.jpg "Forte Payment Systems") ![Hallway Studios](http://email.hallwaystudios.com/logo-height80.gif "Hallway Studios")

# Forte WHMCS eCheck Payment Module

Forte WHMCS Payment module was developed by [Hallway Studios](http://www.hallwaystudios.com) on behalf of [Forte Payment Systems](http://www.forte.net)

This module is designed to handle and process Forte eCheck payments using an external checkout solution. Once setup, the eCheck will handle all
payments for the associated client. This significantly reduces admin and increases usability for your customers.

## Installation
1. Upload the contents of the ``ForteACH_WHMCS`` directory in this package to the root of your WHMCS installation
2. If installing on WHMCS 5.2.x or greater, you will need to change the line that reads ``$version52compat = FALSE;`` to ``$version52compat = TRUE;``. We recommend doing this before installation.
3. Once uploaded, the module can be activated from within your WHMCS installation under Setup > Payment Gateways. Select 'Forte eCheck' from the dropdown list and click 'Activate'
4. You will need to supply all relevant parameters for the installation to work correctly. Invalid details will cause undesired effects and may prevent the module from working altogether. If you find the module is not working, you should check the WHMCS gateway log for debug information.

### Parameters
1. ``Merchant ID`` - The ID assigned to your account by Forte on signup. This ID is also required to login to the NewVT system
2. ``API Login ID`` - This key is generated within the NewVT system under Gateway Settings > Gateway Key
3. ``Secure Transaction Key`` - This key is generated within the NewVT system under Gateway Settings > Gateway Key
4. ``Transaction Password`` - This key is unique and allows the use of AGI interface requests and is not the same as the Secure Transaction Key. This Password is given to you on merchant approval
5. ``Convert To For Processing`` - This converts the currency of invoices to the specified currency. We recommend changing this to **USD** which will allow the module to handle invoices using any currency

### Requirements
1. You must set your ``Return Page`` within the Forte NewVT system to the URL specified in the **IMPORTANT** information when setting up the WHMCS Payment Gateway.
2. It is important to ensure all Gateway Setting fields are populated (exc Sandbox fields, unless you are testing the module) otherwise **the module will not work**

### Recommendations
1. It is suggested to set the 'Convert To For Processing' value to 'USD' in the WHMCS Gateway Settings
2. To avoid confusion with customers, we recommend changing the subject line of the *Credit Card Payment Confirmation* and *Credit Card Payment Failed* email templates

### Usage
Once the module is setup, an invoice with the payment method setup to use ``Forte eCheck`` will display a button allowing the user to setup an eCheck payment. Clicking this button will take the user to the
Forte eCheck setup page. Once successfully setup, all invoices raised will have payment taken automatically using the eCheck module. Any payment failures will be logged in the 'Gateway Log' within WHMCS.
Payment capture can be attempted manually on a per invoice basis using the 'Attempt Capture' button within the WHMCS admin. For automatic captures, the WHMCS cron must be correctly configured to run daily. 
Capture failures will result in a *Credit Card Payment Failed* email to the client while successful captures will send a *Credit Card Payment Confirmation* email to the client. This is default WHMCS behaviour 
and we recommend changing the subject line of these emails slightly within WHMCS using the 'Setup > Email Templates' option to something more relevant such as 'Automatic Payment Confirmation'.

### Client Checkout Process
1. The client views their invoice with the payment method set to ``Forte eCheck``. They then use the appropriate button that is made available to be redirected to Forte Payment systems where their eCheck details can be entered. All other relevant details relating to the client will be supplied to Forte by WHMCS.
2. **SSL ONLY:** Once the client has entered their valid eCheck details, the transaction is processed and they are sent back to the WHMCS system along with all relevant data which is instantly processed and they are redirected back to the paid WHMCS Invoice.
3. **NON-SSL ONLY:** If no SSL connection is available, the client will be redirected to a payment overview within the Forte Payments system with an option to return back to the WHMCS system, e.g. ``Return to Hosting Co.``. In the meantime, data is sent back to the WHMCS hander using an asynchronous POST method so on return the WHMCS Invoice will already be marked as paid and no data is directly transmitted via the users insecure connection.
4. All future Invoices generated against the clients will display a message suggesting the payment will be taken automatically. They will not be given an opportunity to setup another eCheck unless the previous eCheck is deleted from the Forte Payments system and the WHMCS automation cron has had time to recognise this.
5. When the WHMCS automation cron runs, it will attempt to capture payment for any outstanding invoices on the clients account. Successful captures will mark the Invoice as paid while failures will be logged to the Gateway Log. A failure suggesting the eCheck instruction has been cancelled will delete the eCheck instruction from the clients account within WHMCS so future payments will not be taken automatically unless a new eCheck instruction is setup.

### Feature enhancements and suggestions
Please forward all enhancements/ suggestions to [Forte Payment Systems](http://www.forte.net) who can contact [Hallway Studios](http://www.hallwaystudios.com) accordingly. This is a publicly available repository and may be forked.
All functionality will be reviewed thoroughly before entering the main branch.