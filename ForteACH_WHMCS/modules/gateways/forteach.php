<?php

    /**
    * Forte ACH Payment Module
    * Primary Controller
    *
    * @author Hallway Studios <technical@hallwaystudios.com>
    * @version 1.0
    */
    
    /**
    * Load Forte Class
    */
    require_once('forteach/Forte.class.php');
    
    /**
    * Method to access WHMCS localAPI methods. Use this with EXTREME care
    * 
    * @param string $command Command to execute (all commands are the file names of files in /includes/api/)
    * @param array $aValues Array of values to parse
    */
    function forteach_api($command, $aValues) {
        
        # loop through each value and set as request and POST
        # this is to integrate with the WHMCS API
        foreach ($aValues as $key => $value) {
            $$key = $_REQUEST[$key] = $_POST[$key] = $value;
        }
        
        # load the relevant API file
        $command = strtolower($command);
        if (file_exists(ROOTDIR . "/includes/api/" . $command . ".php")) {
            
            # load in API file and return results (stored in $apiresults)
            require(ROOTDIR . "/includes/api/" . $command . ".php");
            
            # unset all keys
            foreach ($aValues as $key => $value) {
                unset($$key,$_REQUEST[$key],$_POST[$key]);
            }
            
            # return results
            return $apiresults;
            
        } else {
            # could not load API command, return error
          return array("result" => "error", "message" => "Invalid API Command");
        }
    }
    
    /**
    ** Forte ACH configuration for WHMCS
    ** This method is used by WHMCS to establish the configuration information
    ** used within the admin interface. These params are then stored in `tblpaymentgateways`
    **/
    function forteach_config() {

        global $CONFIG;

        $aConfig = array(
            'FriendlyName' => array(
                'Type' => 'System',
                'Value' => 'Forte eCheck'
            ),
            'UsageNotes' => array(
                'Type' => 'System',
                'Value' => '<strong style="text-decoration: underline">IMPORTANT:</strong> You must set your <strong>Return Page 1</strong> within 
                            the \'Gateway Settings > Secure Web Pay > Main\' tab on Forte to <strong>'.forteach_sslurl().'/modules/gateways/forteach/redirect.php</strong>.<br />
                            We recommend setting \'Accept Scheduled Transactions\' to \'No\'. Your installation should also be encrypted by SSL.'
            ),
            'merchant_id' => array(
                'FriendlyName' => 'Merchant ID',
                'Type' => 'text',
                'Size' => '15',
                'Description' => 'Your Merchant ID as provided to you by Forte Payment Systems'
            ),
            'api_login_id' => array(
                'FriendlyName' => 'API Login ID',
                'Type' => 'text',
                'Size' => 30,
                'Description' => 'Can be generated from User Panel > Gateway Settings > Gateway Key'
            ),
            'api_transaction_key' => array(
                'FriendlyName' => 'Secure Transaction Key',
                'Type' => 'text',
                'Size' => 30,
                'Description' => 'Can be generated from User Panel > Gateway Settings > Gateway Key'
            ),
            'api_transaction_pass' => array(
                'FriendlyName' => 'Transaction Password',
                'Type'         => 'text',
                'Size'         => 30,
                'Description'  => 'This should be supplied when you setup your merchant account and <strong>is different to the Secure Transaction Key</strong>.'  
            ),
            'dev_merchant_id' => array(
                'FriendlyName' => 'Sandbox Merchant ID',
                'Type' => 'text',
                'Size' => '15',
                'Description' => 'Your Sandbox Merchant ID as provided to you by Forte Payment Systems'
            ),
            'dev_api_login_id' => array(
                'FriendlyName' => 'Sandbox API Login ID',
                'Type' => 'text',
                'Size' => 30,
                'Description' => 'Can be generated from Sandbox User Panel > Gateway Settings > Gateway Key'
            ),
            'dev_api_transaction_key' => array(
                'FriendlyName' => 'Sandbox Secure Transaction Key',
                'Type' => 'text',
                'Size' => 30,
                'Description' => 'Can be generated from SandBox User Panel > Gateway Settings > Gateway Key'
            ),
            'dev_api_transaction_pass' => array(
                'FriendlyName' => 'Sandbox Transaction Password',
                'Type'         => 'text',
                'Size'         => 30,
                'Description'  => 'This should be supplied when you setup your merchant account and <strong>is different to the Secure Transaction Key</strong>.'  
            ),
            'button_value' => array(
                'FriendlyName' => 'eCheck Setup Button Value',
                'Description' => 'The value of the button a user presses to setup an instruction',
                'Type' => 'text',
                'Size' => 80,
                'Value' => 'Setup automatic eCheck Payment'
            ),
            'ach_active_msg' => array(
                'FriendlyName' => 'eCheck Active Message',
                'Description' => 'The message to display to a user when their invoice has an active eCheck',
                'Type' => 'text',
                'Size' => 80,
                'Value' => 'This invoice will be paid on the due date using eCheck under your existing agreement'
            ),
            'test_mode' => array(
                'FriendlyName' => 'Sandbox Mode',
                'Type' => 'yesno',
                'Description' => 'Tick to enable the Forte Payment Systems Sandbox Environment'
            )
        );
        
        # create GoCardless DB if it hasn't already been created
        forteach_createdb();

        # return configuration params
        return $aConfig;

    }
    
    /**
    * Get WHMCS installation SSL URL
    * 
    * @return string System URL (either SSL or insecure)
    */
    function forteach_sslurl() {
        global $CONFIG;
        return ($CONFIG['SystemSSLURL'] ? $CONFIG['SystemSSLURL'] : $CONFIG['SystemURL']);
    }

    /**
    * Checks whether test mode is enabled or disabled
    * and sets appropriate details against GoCardless object
    * @param array $params Array of parameters that contains gateway details
    */
    function forteach_set_account_details($params=null) {

        # check we have been able to obtain the correct params
        if(!isset($params['merchant_id'])) {
            throw new Exception('Could not get Forte ACH paramaters');
        }

        # check if we are running in Sandbox mode (test_mode)
        if($params['test_mode'] == 'on') {
            
            # Initialise SANDBOX Account Details
            Forte::$environment = 'sandbox';
            Forte::set_account_details(array(
                'merchant_id'           => $params['dev_merchant_id'],
                'user_id'               => $params['dev_user_id'],
                'login_id'              => $params['dev_api_login_id'],
                'transaction_key'       => $params['dev_api_transaction_key'],
                'transaction_password'  => $params['dev_api_transaction_pass']
            ));
            
        } else {
            
            # Initialise PRODUCTION Account Details
			Forte::$environment = 'production';
            Forte::set_account_details(array(
                'merchant_id'           => $params['merchant_id'],
                'user_id'               => $params['user_id'],
                'login_id'              => $params['api_login_id'],
                'transaction_key'       => $params['api_transaction_key'],
                'transaction_password'  => $params['api_transaction_pass']
            ));
                
        }
    }

    /**
    ** Builds the payment link for WHMCS users to be redirected to Forte Payment Systems
    **/
    function forteach_link($params) {
        
        # check the user is in US
        if($params['clientdetails']['countrycode'] <> 'US') {
            return '<p><strong>Sorry, this payment method is only avaiable for customers within the United States.</strong></p>';
            
        # check if there is an existing active instruction, if there is we will output a relevant message
        } elseif(mysql_num_rows(select_query('mod_forteach_clients','ID',array('TokenID' => array('sqltype' => 'NEQ', 'value' => ''), 'WHMCSID' => $params['clientdetails']['userid'])))) {
            return "<p><strong>".$params['ach_active_msg']."</strong></p>";
        }
        
        #########################################################
        ### IF WE REACH THIS POINT, WE CAN SETUP A NEW ECHECK ###
        #########################################################
        
        # instantiate ACH account details
        forteach_set_account_details($params);
        
        # store the UTC timestamp in a variable as we dont want this to change during processing
        date_default_timezone_set('UTC');
        $sDate = forteach_ticks();
        
        # determine the return method (ASyncPOST or POST)
        if(strstr(forteach_sslurl(),'https://')) {
            // SSL is enabled so we can post straight back to the redirect controller
            $sReturnParams = "<input type='hidden' name='pg_return_method' value='Post' />";
        } else {
            // we will use AsyncPost to return the data
            $sReturnParams = "<input type='hidden' name='pg_return_method' value='AsyncPost' />".
                             "<input type='hidden' name='pg_continue_url' value='".forteach_sslurl()."/viewinvoice.php?id=".(int)$params['invoiceid']."' />".
                             "<input type='hidden' name='pg_continue_description' value='Return to " . forteach_sanitise($params['companyname']) . "' />";
        }
        
        # build the button for form submission and return it. This will be output to the browser
        return "<p>
                    <form name='ach_submit' action='".Forte::url('swp')."' method='POST'>
                        <input type='hidden' name='pg_billto_postal_name_company' value='".forteach_sanitise($params['clientdetails']['companyname'])."' />
                        <input type='hidden' name='pg_billto_postal_name_first' value='".forteach_sanitise($params['clientdetails']['firstname'])."' />
                        <input type='hidden' name='pg_billto_postal_name_last' value='".forteach_sanitise($params['clientdetails']['lastname'])."' />
                        <input type='hidden' name='pg_billto_postal_street_line1' value='".forteach_sanitise($params['clientdetails']['address1'])."' />
                        <input type='hidden' name='pg_billto_postal_street_line2' value='".forteach_sanitise($params['clientdetails']['address2'])."' />
                        <input type='hidden' name='pg_billto_postal_city' value='".forteach_sanitise($params['clientdetails']['city'])."' />
                        <input type='hidden' name='pg_billto_postal_stateprov' value='".forteach_sanitise($params['clientdetails']['state'])."' />
                        <input type='hidden' name='pg_billto_postal_postalcode' value='".forteach_sanitise($params['clientdetails']['postcode'])."' />
                        <input type='hidden' name='pg_billto_telecom_phone_number' value='".($params['clientdetails']['phonenumber'])."' />
                        <input type='hidden' name='pg_billto_online_email' value='".forteach_sanitise($params['clientdetails']['email'])."' />
                        <input type='hidden' name='pg_shipto_postal_name' value='".forteach_sanitise($params['clientdetails']['firstname'].' '.$params['clientdetails']['lastname'])."' />
                        <input type='hidden' name='pg_shipto_postal_street_line1' value='".forteach_sanitise($params['clientdetails']['address1'])."' />
                        <input type='hidden' name='pg_shipto_postal_street_line2' value='".forteach_sanitise($params['clientdetails']['address2'])."' />
                        <input type='hidden' name='pg_shipto_postal_city' value='".forteach_sanitise($params['clientdetails']['city'])."' />
                        <input type='hidden' name='pg_shipto_postal_stateprov' value='".forteach_sanitise($params['clientdetails']['state'])."' />
                        <input type='hidden' name='pg_shipto_postal_postalcode' value='".forteach_sanitise($params['clientdetails']['postcode'])."' />
                        <input type='hidden' name='pg_return_url' value='".forteach_sslurl()."/modules/gateways/forteach/redirect.php' />".
                        $sReturnParams. // include return params calculated above
                        "<input type='hidden' name='pg_cancel_url' value='".forteach_sslurl()."/viewinvoice.php?id=".(int)$params['invoiceid']."' />
                        <input type='hidden' name='pg_save_client' value='2' />
                        <input type='hidden' name='pg_scheduled_transaction' value='0' />
                        <input type='hidden' name='pg_api_login_id' value='".Forte::$sLoginID."' />
                        <input type='hidden' name='pg_transaction_type' value='20' />
                        <input type='hidden' name='pg_version_number' value='1.0' />
                        <input type='hidden' name='pg_total_amount' value='".(float)($params['amount'])."' />
                        <input type='hidden' name='pg_utc_time' value='".$sDate."' />
                        <input type='hidden' name='pg_transaction_order_number' value='".(int)$params['invoiceid']."' />
                        <input type='hidden' name='pg_ts_hash' value='".Forte::hmac(Forte::$sLoginID.'|20|1.0|'.(float)$params['amount'].'|'.$sDate.'|'.$params['invoiceid'])."' />
                        <input type='submit' value='".$params['button_value']."' />
                    </form>
                </p>";
    }

    /**
    ** WHMCS method to capture payments
    ** This method is triggered by WHMCS in an attempt to capture a PreAuth payment
    **
    ** @param array $params Array of paramaters parsed by WHMCS
    **/
    function forteach_capture($params) {
        
        # instantiate Forte account details
        forteach_set_account_details($params);
        
        # get invoice data
        $aInvoice = forteach_api('getinvoice',array('invoiceid' => $params['invoiceid']));
        
        # attempt to load client and token
        list($iClientID,$iInstructionID) = mysql_fetch_row(select_query('mod_forteach_clients','ForteID,TokenID',array('WHMCSID' => $aInvoice['userid'])));
        
        # check the client ID exists
        if(!$iClientID) {
            # client has not registered with Forte
            forteach_logTransaction('Payment capture attempt for Invoice #'.$aInvoice['invoiceid'].' failed because no Forte integration has been setup','Error');
            return false;
        }
        
        # attempt to load instruction ID
        if(!$iInstructionID) {
            # client does not have an active instruction with Forte
            forteach_logTransaction('Payment capture attempt for Invoice #'.$aInvoice['invoiceid']. ' failed because no Forte payment instruction exists','Error');
            return false;
        }
        
        # build up data to send to Forte
        $aData = array('pg_merchant_id'         => Forte::$sMerchantID,
                       'pg_password'            => Forte::$sTransactionPass,
                       'pg_transaction_type'    => '20',
                       'pg_total_amount'        => (float)$aInvoice['balance'],
                       'pg_client_id'           => $iClientID,
                       'pg_payment_method_id'   => $iInstructionID);
                       
        # attempt to create the transaction using cURL
        $ch = curl_init(Forte::cURLurl());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($aData).'&endofdata');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        # execute cURL request and return cleaned data
        $aData = curl_exec($ch);
        $aData = str_replace("\n",'&',str_replace('endofdata','',trim($aData)));
        parse_str($aData,$aData);
        
        # check if the payment has been approved or not
        if($aData['pg_response_description'] == 'APPROVED') {
            
            # all done, log the transaction as a success
            return array('status' => 'success', 'transid' => $aData['pg_authorization_code'],'rawdata' => print_r($aData,true));
            
        } elseif($aData['pg_response_description'] == 'INVALID TOKEN') {
            
            # invalid payment method, we will assume the payment method has been cancelled
            forteach_logTransaction('Could not find eCheck authorisation, cancelled: ' . print_r($aData,true),'Auth Cancelled');
            
            # get the original gateway and store it as a variable
            $aFilter = array('TokenID' => $iInstructionID);
            list($sOriginalGateway) = mysql_fetch_row(select_query('mod_forteach_clients','OriginalGateway',$aFilter,null,null,1));
            
            # attempt to delete the instruction from the database, this will stop automatic captures
            if(delete_query('mod_forteach_clients',$aFilter)) {
                # restore the original gateway details on the users account
                forteach_updategateway($aInvoice['userid'],$sOriginalGateway);
            }
            
            # return false as the capture failed
            return false;
            
        } else {
            
            # payent failed, log it and return false
            forteach_logTransaction('Payment capture for invoice #'.$aInvoice['invoiceid']. ' failed: ' . print_r($aData,true));
            return false;
            
        }
    }

    /**
    ** Supress credit card request on checkout
    **/
    function forteach_nolocalcc() {}
    
    /**
    * Sanitises data output to web forms
    * 
    * @return string Sanitised string
    */
    function forteach_sanitise($data) {
        return str_replace("'",'&#39;',htmlentities($data,null,'UTF-8'));
    }
    
    
    /**
    * Calculate number of ticks (measure used for Forte time)
    * 
    * @param timestamp $timestamp UNIX timestamp
    */
    function forteach_ticks($timestamp=null) {
        
        # if the timestamp is empty, use current timestamp
        if(is_null($timestamp)) $timestamp = time();
        
        # calculate eticks
        return number_format(($timestamp * 10000000) + 621355968000000000,0,'.','');
    }

    /**
    * Mass updates users gateway preference on unpaid invoices, invoice items, domains, hosting, addons etc
    * 
    * @param int $clientID WHMCS client ID
    * @param string $gatewayName New gateway name. Defaults to 'forteach'
    */
    function forteach_updategateway($clientID,$gatewayName='forteach') {
        
        # get all users unpaid invoices
        $result = select_query('tblinvoices','id',array('userid' => $clientID,'status' => 'Unpaid', 'paymentmethod' => array('sqltype' => 'NEQ','value' => $gatewayName)));
        
        $aInvoices = array();
        while($data = mysql_fetch_array($result)) {
            $aInvoices[] = (int)$data[0];
        }
        
        # update invoices where invoice ID is one of the above and status is still unpaid
        mysql_query('UPDATE tblinvoices SET paymentmethod = \''.$gatewayName.'\' WHERE id IN ('.join(',',$aInvoices).') AND status = \'Unpaid\'');
        mysql_query('UPDATE tblinvoiceitems SET paymentmethod = \''.$gatewayName.'\' WHERE invoiceid IN ('.join(',',$aInvoices).')');
        unset($aInvoices,$data);
        
        # update domains
        update_query('tbldomains',array('paymentmethod' => $gatewayName),array('userid' => $clientID));
        
        # update client default payment gateway
        update_query('tblclients',array('paymentmethod' => $gatewayName, 'defaultgateway' => $gatewayName),array('id' => $clientID));
        
        # get all hosting services for this user
        $result = select_query('tblhosting','id',array('userid' => $clientID));
        
        $aHosting = array();
        while($data = mysql_fetch_array($result)) {
            $aHosting[] = (int)$data[0];
        }
        
        # update hosting and hosting addons tables
        mysql_query('UPDATE tblhosting SET paymentmethod = \''.$gatewayName.'\' WHERE id IN ('.join(',',$aHosting).')');
        mysql_query('UPDATE tblhostingaddons SET paymentmethod = \''.$gatewayName.'\' WHERE hostingid IN ('.join(',',$aHosting).')');
        unset($aHosting,$data);
        
        return true;
    }

    /**
    ** Display payment status message to admin when the preauth
    ** has been setup but the payment is incomplete
    **/
    function forteach_adminstatusmsg($params) {
        
        # if the invoice is unpaid
        if ($params['status'] == 'Unpaid') {
            
            # get the users details and store in array $aUser
            $aUser = forteach_api('getclientsdetails',array('clientid' => $params['userid']));
            
            # check if the payment will be taken automatically
            if(mysql_num_rows(select_query('mod_forteach_clients','ID',array('TokenID' => array('sqltype' => 'NEQ', 'value' => ''), 'WHMCSID' => $params['userid'])))) {
                return array('type' => 'info', 'title' => 'Active eCheck', 'msg' => 'This invoice has an active ACH instruction and payment will be taken automatically.');
                
            # check the user is in the US
            } elseif($aUser['countrycode'] <> 'US') {
                $sError = 'The selected gateway for this invoice does not support the users country ('.$aUser['countrycode'].'). Only US is supported';
                return array('type' => 'error', 'title' => 'Payment Gateway Not Supported', 'msg' => $sError);
                
            # if we get to this point, the user cant have an existing eCheck
            } else {
                $sError = 'This user does not have an active eCheck instruction so automatic billing will not work.';
                return array('type' => 'error', 'title' => 'No active eCheck instruction', 'msg' => $sError);
            }
        }
    }
    
    /**
    * Currency conversion method to enhance functionality of WHMCS convertCurrency method
    * 
    * @param float $amount Value to convert
    * @param int $sourceCurrencyID ID of the source currency (currency the value is currently in)
    * @param mixed $targetCurrencyID ID of the target currency (desired output currency)
    * @return float Value after currency conversion has completed
    */
    function forteach_currencyConvert($amount,$sourceCurrencyID,$targetCurrencyID) {
		
		# sanitise the currency, strip anything we dont need out
		$amount = forteach_sanitiseCurrency($amount);
	
        if(($sourceCurrencyID == $targetCurrencyID) || empty($sourceCurrencyID)) {
            # return the amount given
            return $amount;
        } else {
            # return the converted currency
            return convertCurrency($amount,$sourceCurrencyID,$targetCurrencyID);
        }
    }
    
    /**
    * Strips out 'invalid' values for currencies
    * 
    * @param string $value Currency such as �1,800 or 1,800.20
    */
    function forteach_sanitiseCurrency($value) {
        return preg_replace('/[^0-9.]/','',$value);
    }
    
    /**
    * Override to the standard WHMCS logTransaction as this does not always work!
    * 
    * @param string $data
    * @param string $result
    */
    function forteach_logTransaction($data, $result='Error') {
        return insert_query('tblgatewaylog',array('date' => date('Y-m-d H:i:s'),'gateway' => 'Forte ACH', 'data' => $data, 'result' => $result));
    }
    
    /**
    ** Create appropriate schema if it does not already exist
    **/
    function forteach_createdb() {
        # check the main table exists (mod_forteach_clients)
        if(mysql_num_rows(full_query('SHOW TABLES LIKE \'mod_forteach_clients\''))) {
            # installation already complete
            return true;
        } else {
            # CREATE table mod_forteach_clients
            full_query('CREATE TABLE `mod_forteach_clients` (`ID` int(10) unsigned NOT NULL auto_increment,`WHMCSID` int(10) unsigned NOT NULL,`ForteID` int(10) unsigned NOT NULL,`TokenID` int(10) NOT NULL,`OriginalGateway` varchar(20) collate utf8_unicode_ci NOT NULL,PRIMARY KEY  (`ID`),UNIQUE KEY `WHMCSID` (`WHMCSID`),UNIQUE KEY `ForteID` (`ForteID`),UNIQUE KEY `TokenID` (`TokenID`)) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
            return true;
        }
    }
    
    /**
    * Perform a redirect to invoice $invoiceID
    * 
    * @param int $invoiceID ID of the invoice to redirect to
    */
    function forteach_invoiceRedirect($invoiceID) {
        header('HTTP/1.1 303 See Other');
        header('Location: '.forteach_sslurl().'/viewinvoice.php?id='.(int)$invoiceID);
        exit;
    }