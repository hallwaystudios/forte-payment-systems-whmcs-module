<?php
    
    /**
    * Basic PHP class to handle correspondance with Forte
    * @author Hallway Studios <technical@hallwaystudios.com>
    * 
    * @version 1.0
    */
    class Forte {
        
        static public
            /**
            * Environment either sandbox or production. Set as sandbox by default
            * 
            * @var string
            */
            $environment = 'sandbox',
            /**
            * Secure transaction ID. Required for API interaction and used to generate HMAC
            * 
            * @var string Secure Transaction Key
            */
            $sTransactionKey = null,
            /**
            * Transaction password for AGI transactions
            * 
            * @var string Transaction Password
            */
            $sTransactionPass = null,
            /**
            * Merchant ID
            * 
            * @var string Merchant ID
            */
            $sMerchantID = null,
            /**
            * Merchant Login ID
            * 
            * @var string Merchant login ID
            */
            $sLoginID = null;
            
            
        static private
            $sCheckoutSanboxURL         = 'https://sandbox.paymentsgateway.net/swp/co/default.aspx',
            $sCheckoutProductionURL     = 'https://swp.paymentsgateway.net/co/default.aspx',
            $sCURLSandboxURL            = 'https://www.paymentsgateway.net/cgi-bin/posttest.pl',
            $sCURLProductionURL         = 'https://www.paymentsgateway.net/cgi-bin/postauth.pl';
        
        /**
        * Stores the account details against the object
        * 
        * @param mixed $aDetails
        */
        public static function set_account_details(array $aDetails) {
            
            # set variables
            self::$sTransactionKey = $aDetails['transaction_key'];
            self::$sTransactionPass = $aDetails['transaction_password'];
            self::$sMerchantID = $aDetails['merchant_id'];
            self::$sLoginID = $aDetails['login_id'];
            
            return true;
        }
        
        /**
        * Gets URL based on current environment
        * 
        */
        public static function url() {
            if(self::$environment == 'production') {
                # return production URL
                return self::$sCheckoutProductionURL;
            } else {
                # return Sandbox URL
                return self::$sCheckoutSanboxURL;
            }
        }
        
        /**
        * URL to use in correspondance with cURL requests
        * 
        */
        public static function cURLurl() {
            if(self::$environment == 'production') {
                # return production URL
                return self::$sCURLProductionURL;
            } else {
                # return sandbox URL
                return self::$sCURLSandboxURL;
            }
        }
        
        /**
        * RFC 2104 HMAC implementation for PHP
        * Creates an MD5 HMAC.
        * Eliminates the need to install mhash to compute a HMAC
        * 
        * @author Lance Rushing
        * @param string $data Data to encrypt
        * @param string $key Secure transaction key
        */
        public static function hmac ($data, $key = null) {
            
            # check if the key is null
            if(is_null($key)) {
                $key = self::$sTransactionKey;
            }
            
            $b = 64; // byte length for md5
            
            if (strlen($key) > $b) $key = pack("H*",md5($key));
            
            $key = str_pad($key, $b, chr(0x00));
            $ipad = str_pad('', $b, chr(0x36));
            $opad = str_pad('', $b, chr(0x5c));
            $k_ipad = $key ^ $ipad ;
            $k_opad = $key ^ $opad;

            # return resulting string in MD5
            return md5($k_opad . pack("H*",md5($k_ipad . $data)));
        }
        
    }
    
    class ForteAuth {
        public $APILoginID;
        public $SecureTransactionKey;
    }