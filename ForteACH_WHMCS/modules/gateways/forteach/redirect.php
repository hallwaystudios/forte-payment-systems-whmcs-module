<?php
    
    /**
    * Forte ACH Payment Module
    * Redirect controller - handles users returning from making payment
    * 
    * @author Hallway Studios <technical@hallwaystudios.com>
    * @version 1.0
    */
    
    # load all required files
    $version52compat = FALSE;
    $whmcsdir = dirname(__FILE__) . '/../../../';
    if($version52compat) {
        require_once $whmcsdir . 'init.php';
    } else {
        require_once $whmcsdir . 'dbconnect.php';
        require_once $whmcsdir . '/includes/functions.php';
    }
    require_once $whmcsdir . '/includes/gatewayfunctions.php';
    require_once $whmcsdir . '/includes/invoicefunctions.php';
    require_once $whmcsdir . '/modules/gateways/forteach.php';
    
    # get gateway params
    $gateway = getGatewayVariables('forteach');
    
    # check the gateway is active
    if(!$gateway['type']) die('ForteACH module not activated');
    
    # set the Forte ACH account details
    forteach_set_account_details($gateway);
    
    # verify the HMAC key matches what we would expect (SANITY CHECK TO PREVENT INVALID REQS)
    if($_POST['pg_ts_hash_response'] <> Forte::hmac(Forte::$sLoginID.'|'.$_POST['pg_trace_number'].'|'.$_POST['pg_total_amount'].'|'.$_POST['pg_utc_time'])) {
        # log the error so we have logs should this be a genuine request!
        forteach_logTransaction("Callback received but authentication failed\r\n\r\nPOST DATA:".print_r($_POST,true),'Authentication Error');
        header('HTTP/1.1 403 Forbidden'); exit('Authorisation failed');
    }
    
    # check to ensure the transaction has been approved
    if($_POST['pg_response_description'] == 'APPROVED') {
        
        # get the user ID and store it in $iInvoiceID
        $iInvoiceID = $_POST['pg_transaction_order_number'];
        
        # verify the invoice ID is valid, if not the script will exit out
        checkCbInvoiceID($iInvoiceID,$gateway['paymentmethod']);
        
        # get invoice and client details
        $aInvoice = forteach_api('getinvoice',array('invoiceid' => $iInvoiceID));
        $aUser = forteach_api('getclientsdetails',array('clientid' => $aInvoice['userid']));
        
        # check the transaction hasnt already been processed
        foreach($aInvoice['transactions']['transaction'] as $aTransaction) {
            if($aTransaction['transid'] == $_POST['pg_authorization_code']) {
                
                # this transaction has already been processed, log error
                forteach_logTransaction(sprintf('Invoice #%s payment failed. Transaction #%s already exists',$iInvoiceID,$aTransaction['transid']),'Error');
                
                # redirect user to invoice and exit out
                forteach_invoiceRedirect($iInvoiceID);
            }
        }
        
        # unset invoice
        unset($aInvoice);
        
        # set the gateway ID (remote token if not already done so)
        list($gatewayID,$defaultGateway) = mysql_fetch_row(select_query('tblclients','gatewayid,defaultgateway',array('id' => $aUser['userid'])));
        
        if(empty($gatewayID) || $gatewayID <> $gateway['paymentmethod']) {
            update_query('tblclients',array('gatewayid' => $gateway['paymentmethod']),array('id' => $aUser['userid']));
        }
        
        # determine original payment gateway to store
        if(strlen($defaultGateway) < 1) {
            $defaultGateway = $gateway['paymentmethod'];
        }
        
        # build array of data to store
        $aData = array('WHMCSID' => $aUser['userid'],
                       'ForteID' => $_POST['pg_client_id'],
                       'TokenID' => $_POST['pg_payment_method_id'],
                       'OriginalGateway' => $defaultGateway);
                       
        # run the query we have built to CREATE/UPDATE the client record
        if(!insert_query('mod_forteach_clients',$aData)) {
            
            # the MySQL query failed, log the transaction and exit out
            forteach_logTransaction('Forte ACH Redirect Failed (database error when saving client): ' . print_r(mysql_error(),true),'Error');
            header('HTTP/1.1 503 Service Unavailable');
            exit('Your request could not be completed');
            
        } else {
            
            ###########################################################
            ### SUCCESS, ALL DATA HAS BEEN RECORDED TO THE DATABASE ###
            ###########################################################
            
            # unset vars not needed
            unset($aData,$gatewayID,$defaultGateway,$aInvoice);
            
            # convert the amount being received to the correct currency
            $_POST['pg_total_amount'] = forteach_currencyConvert($_POST['pg_total_amount'],$gateway['convertto'],$aUser['currency']);
            
            # add invoice payment and update the users global preferences
            addInvoicePayment($iInvoiceID,$_POST['pg_authorization_code'],$_POST['pg_total_amount'],0,$gateway['paymentmethod']);
            forteach_updategateway($aUser['userid']);
            
            # redirect the user back to their paid invoice and exit out
            forteach_invoiceRedirect($iInvoiceID);
            
        }
            
    } else {
        
        # log the transaction as it was not APPROVED and store it to the database
        forteach_logTransaction('Transaction was not APPROVED: ' . print_r($_POST,true),'Error');
        
        # redirect the user to their invoice and exit out
        if($iInvoiceID) forteach_invoiceRedirect($iInvoiceID);
        
    }
    
    header('HTTP/1.1 503 Service Unavailable');
    exit('Your request could not be completed');